package com.antcode.d3ity.shakenram.desktop;

import com.antcode.d3ity.shakenram.ShakeNRam;
import com.antcode.d3ity.shakenram.utils.Constants;
import com.antcode.d3ity.shakenram.utils.GameEventListener;
import com.antcode.d3ity.shakenram.utils.NetMessage;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {

    public static void main(String[] arg) {

        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = Constants.APP_WIDTH;
        config.height = Constants.APP_HEIGHT;
        new LwjglApplication(new ShakeNRam(gameEvent), config);
    }

    private static GameEventListener gameEvent = new GameEventListener() {
        @Override
        public void onGameWaiting() {

        }

        @Override
        public void onGameClosed() {

        }

        @Override
        public void onGameFinish(NetMessage winner) {

        }

        @Override
        public void onCharacterSelect() {

        }
    };
}