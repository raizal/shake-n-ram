package com.antcode.d3ity.shakenram.stages;

import com.antcode.d3ity.shakenram.actors.Background;
import com.antcode.d3ity.shakenram.actors.CountDownTime;
import com.antcode.d3ity.shakenram.actors.Enemy;
import com.antcode.d3ity.shakenram.actors.Ground;
import com.antcode.d3ity.shakenram.actors.Runner;
import com.antcode.d3ity.shakenram.actors.Runner2;
import com.antcode.d3ity.shakenram.actors.Wall;
import com.antcode.d3ity.shakenram.actors.menu.AboutLabel;
import com.antcode.d3ity.shakenram.actors.menu.GameLabel;
import com.antcode.d3ity.shakenram.actors.menu.MusicButton;
import com.antcode.d3ity.shakenram.actors.menu.PauseButton;
import com.antcode.d3ity.shakenram.actors.menu.PausedLabel;
import com.antcode.d3ity.shakenram.actors.menu.SoundButton;
import com.antcode.d3ity.shakenram.actors.menu.StartButton;
import com.antcode.d3ity.shakenram.actors.menu.Winner;
import com.antcode.d3ity.shakenram.enums.GameState;
import com.antcode.d3ity.shakenram.utils.AudioUtils;
import com.antcode.d3ity.shakenram.utils.BodyUtils;
import com.antcode.d3ity.shakenram.utils.Constants;
import com.antcode.d3ity.shakenram.utils.GameManager;
import com.antcode.d3ity.shakenram.utils.NetMessage;
import com.antcode.d3ity.shakenram.utils.PlayerNetData;
import com.antcode.d3ity.shakenram.utils.ShakeData;
import com.antcode.d3ity.shakenram.utils.WorldUtils;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import java.io.IOException;

public class MenuStage extends Stage implements ContactListener {

    private static final int VIEWPORT_WIDTH = Constants.APP_WIDTH;
    private static final int VIEWPORT_HEIGHT = Constants.APP_HEIGHT;

    private World world;
    private Ground ground;
    private Runner runner;
    private Runner2 runner2;

    private final float TIME_STEP = 1 / 300f;
    private float accumulator = 0f;

    private OrthographicCamera camera;

    private Rectangle screenLeftSide;
    private Rectangle screenRightSide;

    private SoundButton soundButton;
    private MusicButton musicButton;
    private PauseButton pauseButton;
    private StartButton startButton;

    private CountDownTime countDownTime;
    private float totalTimePassed;
    private boolean tutorialShown;

    private Winner win;

    private Vector3 touchPoint;

    private GameLabel gameLabel;

    public MenuStage() {
        super(new ScalingViewport(Scaling.stretch, VIEWPORT_WIDTH, VIEWPORT_HEIGHT,
                new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)));

        GameManager.dataEvent = new GameManager.IncomingData() {
            @Override
            public void onData(ShakeData data) {
                if (countDownTime != null && countDownTime.isCountOver()) {
                    if (GameManager.getInstance().getGameState() == GameState.RUNNING) {
                        if (runner.getPlayerName().equals(data.from)) {
                            runner.ram();
                        } else if (runner2.getPlayerName().equals(data.from)) {
                            runner2.ram();
                        }
                    }
                }
            }

            @Override
            public void onJoin(PlayerNetData data) {
                if (runner == null)
                    setUpRunner(data.name, data.characterName);
                else if (runner2 == null)
                    setUpRunner2(data.name, data.characterName);
            }

            @Override
            public void onLeave(PlayerNetData data) {
                if (runner != null && runner.getPlayerName().equals(data.name)) {
                    runner.remove();
                    runner = null;
                } else if (runner2 != null && runner2.getPlayerName().equals(data.name)) {
                    runner2.remove();
                    runner2 = null;
                }
            }

        };

        setUpCamera();
        setUpStageBase();
        setUpTouchControlAreas();
        Gdx.input.setInputProcessor(this);
        AudioUtils.getInstance().init();
        if (Gdx.app.getType() == Application.ApplicationType.Android)
            switchState(GameState.PICK_MODE);
        else
            switchState(GameState.MAIN_MENU);
    }

    private void setUpStageBase() {
        setUpWorld();
        setUpFixedMenu();
    }

    private void setUpGameLabel() {
        if (gameLabel == null) {
            Rectangle gameLabelBounds = new Rectangle(0, getCamera().viewportHeight * 7 / 8,
                    getCamera().viewportWidth, getCamera().viewportHeight / 4);
            gameLabel = new GameLabel(gameLabelBounds);
        }
        addActor(gameLabel);
    }

    private void setUpAboutText() {
        Rectangle gameLabelBounds = new Rectangle(0, getCamera().viewportHeight * 5 / 8,
                getCamera().viewportWidth, getCamera().viewportHeight / 4);
        addActor(new AboutLabel(gameLabelBounds));
    }

    /**
     * These menu buttons are always displayed
     */
    private void setUpFixedMenu() {
        setUpSound();
        setUpMusic();
    }

    private void setUpSound() {
        Rectangle soundButtonBounds = new Rectangle(getCamera().viewportWidth / 64,
                getCamera().viewportHeight * 13 / 20, getCamera().viewportHeight / 10,
                getCamera().viewportHeight / 10);
        soundButton = new SoundButton(soundButtonBounds);
        addActor(soundButton);
    }

    private void setUpMusic() {
        Rectangle musicButtonBounds = new Rectangle(getCamera().viewportWidth / 64,
                getCamera().viewportHeight * 4 / 5, getCamera().viewportHeight / 10,
                getCamera().viewportHeight / 10);
        musicButton = new MusicButton(musicButtonBounds);
        addActor(musicButton);
    }

    private void setUpCountDown() {
        Rectangle countDownBound = new Rectangle(0,
                getCamera().viewportHeight / 4, getCamera().viewportWidth,
                getCamera().viewportHeight / 4);
        countDownTime = new CountDownTime(countDownBound);
        addActor(countDownTime);
    }

    private void setUpWinner(String name) {
        Rectangle countDownBound = new Rectangle(0,
                getCamera().viewportHeight / 2, getCamera().viewportWidth,
                getCamera().viewportHeight / 4);
        win = new Winner(countDownBound, name + "\nWin");
        addActor(win);
    }

    private void setUpPause() {
        Rectangle pauseButtonBounds = new Rectangle(getCamera().viewportWidth / 64,
                getCamera().viewportHeight * 1 / 2, getCamera().viewportHeight / 10,
                getCamera().viewportHeight / 10);
        pauseButton = new PauseButton(pauseButtonBounds, new GamePauseButtonListener());
//        addActor(pauseButton);
    }

    /**
     * These menu buttons are only displayed when the game is over
     */
    private void setUpMainMenu() {
        setUpStart();
        if (win != null) {
            win.remove();
        }
        if (runner != null) {
            runner.remove();
            runner = null;
        }
        if (runner2 != null) {
            runner2.remove();
            runner2 = null;
        }

    }

    private void setUpStart() {
        //Setup start button
        System.out.println(getCamera().viewportWidth * 6 / 16 + " " + getCamera().viewportHeight / 4);
        Rectangle startButtonBounds = new Rectangle(
                getCamera().viewportWidth * 6 / 16,
                getCamera().viewportHeight / 4,
                getCamera().viewportWidth / 4,
                (getCamera().viewportWidth / 4) / 786f * 633);
        startButton = new StartButton(startButtonBounds, new GameStartButtonListener());
        addActor(startButton);
    }

    private void setUpWorld() {
        world = WorldUtils.createWorld();
        world.setContactListener(this);
        setUpBackground();
        setUpGround();
    }

    private void setUpBackground() {
        addActor(new Background());
    }

    private void setUpGround() {
        ground = new Ground(WorldUtils.createGround(world));
        addActor(ground);

        addActor(new Wall(WorldUtils.createWallLeft(world)));
        addActor(new Wall(WorldUtils.createWallRight(world)));
    }

    private void setUpCharacters() {
        setUpPauseLabel();
        setUpRunner("RAIZ", Constants.STALP);
        setUpRunner2("NAW", Constants.PANATA);
        //createEnemy();
    }

    private void setUpRunner(String name, String charName) {
        if (runner != null) {
            runner.remove();
        }
        runner = new Runner(WorldUtils.createRunner(world), name, charName);

        runner.getBody().setLinearVelocity(0, 0);
        addActor(runner);
    }

    private void setUpRunner2(String name, String charName) {
        if (runner2 != null) {
            runner2.remove();
        }
        runner2 = new Runner2(WorldUtils.createRunner2
                (world), name, charName);

        runner2.getBody().setLinearVelocity(0, 0);
        addActor(runner2);
    }

    private void setUpCamera() {
        camera = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0f);
        camera.update();
    }

    private void setUpTouchControlAreas() {
        touchPoint = new Vector3();
        screenLeftSide = new Rectangle(0, 0, getCamera().viewportWidth / 2,
                getCamera().viewportHeight);
        screenRightSide = new Rectangle(getCamera().viewportWidth / 2, 0,
                getCamera().viewportWidth / 2, getCamera().viewportHeight);
    }

    private void setUpPauseLabel() {
        Rectangle pauseLabelBounds = new Rectangle(0, getCamera().viewportHeight * 7 / 8,
                getCamera().viewportWidth, getCamera().viewportHeight / 4);
        addActor(new PausedLabel(pauseLabelBounds));
    }

    private void setUpTutorial() {
        if (tutorialShown) {
            return;
        }
        setUpLeftTutorial();
        setUpRightTutorial();
        tutorialShown = true;
    }

    private void setUpLeftTutorial() {
        float width = getCamera().viewportHeight / 4;
        float x = getCamera().viewportWidth / 4 - width / 2;
        Rectangle leftTutorialBounds = new Rectangle(x, getCamera().viewportHeight * 9 / 20, width,
                width);
//        addActor(new Tutorial(leftTutorialBounds, Constants.TUTORIAL_LEFT_REGION_NAME,
//                Constants.TUTORIAL_LEFT_TEXT));
    }

    private void setUpRightTutorial() {
        float width = getCamera().viewportHeight / 4;
        float x = getCamera().viewportWidth * 3 / 4 - width / 2;
        Rectangle rightTutorialBounds = new Rectangle(x, getCamera().viewportHeight * 9 / 20, width,
                width);
//        addActor(new Tutorial(rightTutorialBounds, Constants.TUTORIAL_RIGHT_REGION_NAME,
//                Constants.TUTORIAL_RIGHT_TEXT));
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (runner == null || runner2 == null) return;

        if (GameManager.getInstance().getGameState() == GameState.PAUSED) return;

        if ((countDownTime != null && !countDownTime.isCountOver()))
            return;

        if (GameManager.getInstance().getGameState() == GameState.RUNNING) {

            totalTimePassed += delta;
            updateDifficulty();
        }

        Array<Body> bodies = new Array<Body>(world.getBodyCount());
        world.getBodies(bodies);

        for (Body body : bodies) {
            update(body);
        }

        // Fixed timestep
        accumulator += delta;

        while (accumulator >= delta) {
            world.step(TIME_STEP, 6, 2);
            accumulator -= TIME_STEP;
        }

        if (runner2 != null)
            runner2.isBounced();
        if (runner != null)
            runner.isBounced();
        //TODO: Implement interpolation

    }

    private void update(Body body) {
        if (!BodyUtils.bodyInBounds(body)) {
            if (BodyUtils.bodyIsEnemy(body) && !runner.isHit()) {
                //createEnemy();
            }
            world.destroyBody(body);
        }
    }

    private void createEnemy() {
        Enemy enemy = new Enemy(WorldUtils.createEnemy(world));
        enemy.getUserData().setLinearVelocity(
                GameManager.getInstance().getDifficulty().getEnemyLinearVelocity());
        addActor(enemy);
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {

        // Need to get the actual coordinates
        translateScreenToWorldCoordinates(x, y);

        // If a menu control was touched ignore the rest
        if (menuControlTouched(touchPoint.x, touchPoint.y)) {
            return super.touchDown(x, y, pointer, button);
        }

        if (GameManager.getInstance().getGameState() != GameState.RUNNING) {
            return super.touchDown(x, y, pointer, button);
        }

        if (countDownTime != null && countDownTime.isCountOver()) {
            if (rightSideTouched(touchPoint.x, touchPoint.y)) {
                if (!runner.isHit())
                    runner.ram();
            } else if (leftSideTouched(touchPoint.x, touchPoint.y)) {
                if (!runner2.isHit())
                    runner2.ram();
            }
        }

        return super.touchDown(x, y, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (GameManager.getInstance().getGameState() != GameState.RUNNING) {
            return super.touchUp(screenX, screenY, pointer, button);
        }

        return super.touchUp(screenX, screenY, pointer, button);
    }

    private boolean menuControlTouched(float x, float y) {
        boolean touched = false;

        switch (GameManager.getInstance().getGameState()) {
            case MAIN_MENU:
                touched = startButton.getBounds().contains(x, y);

                if (touched) {
                    if (Gdx.app.getType() == Application.ApplicationType.Android) {
                        switchState(GameState.START);
                    } else
//                        switchState(GameState.START);
                        switchState(GameState.WAITING);
                }
                break;
            case RUNNING:
                break;
            case PAUSED:
                touched = pauseButton.getBounds().contains(x, y);
                break;
            case GAME_OVER:
                touched = win.getBounds().contains(x, y);
                if (touched) {
                    switchState(GameState.MAIN_MENU);
                }
        }

        return touched || soundButton.getBounds().contains(x, y)
                || musicButton.getBounds().contains(x, y);
    }

    private boolean rightSideTouched(float x, float y) {
        return screenRightSide.contains(x, y);
    }

    private boolean leftSideTouched(float x, float y) {
        return screenLeftSide.contains(x, y);
    }

    /**
     * Helper function to get the actual coordinates in my world
     *
     * @param x
     * @param y
     */
    private void translateScreenToWorldCoordinates(int x, int y) {
        getCamera().unproject(touchPoint.set(x, y, 0));
    }

    @Override
    public void beginContact(Contact contact) {

        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        if ((BodyUtils.bodyIsRunner(a) && BodyUtils.bodyIsRunner2(b)) ||
                (BodyUtils.bodyIsRunner2(a) && BodyUtils.bodyIsRunner(b))) {
//            runner.hit();
//            runner2.hit();
        } else if ((BodyUtils.bodyIsRunner(a) && BodyUtils.bodyIsWallLeft(b)) ||
                (BodyUtils.bodyIsWallLeft(a) && BodyUtils.bodyIsRunner(b))) {
            setUpWinner(runner2.getPlayerName());
            switchState(GameState.GAME_OVER);
            if (GameManager.getInstance().getGameEventListener() != null) {
                GameManager.getInstance().getGameEventListener().onGameFinish(new NetMessage("server", "winner:" + runner2.getName()));
            }

        } else if ((BodyUtils.bodyIsRunner2(a) && BodyUtils.bodyIsWallRight(b)) ||
                (BodyUtils.bodyIsWallRight(a) && BodyUtils.bodyIsRunner2(b))) {
            setUpWinner(runner.getPlayerName());
            switchState(GameState.GAME_OVER);
            if (GameManager.getInstance().getGameEventListener() != null) {
                GameManager.getInstance().getGameEventListener().onGameFinish(new NetMessage("server", "winner:" + runner.getName()));
            }
        }

    }

    private void updateDifficulty() {

//        if (GameManager.getInstance().isMaxDifficulty()) {
//            return;
//        }
//
//        Difficulty currentDifficulty = GameManager.getInstance().getDifficulty();
//
//        if (totalTimePassed > GameManager.getInstance().getDifficulty().getLevel() * 5) {
//
//            int nextDifficulty = currentDifficulty.getLevel() + 1;
//            String difficultyName = "DIFFICULTY_" + nextDifficulty;
//            GameManager.getInstance().setDifficulty(Difficulty.valueOf(difficultyName));
//
//            runner.onDifficultyChange(GameManager.getInstance().getDifficulty());
//        }

    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    private class GamePauseButtonListener implements PauseButton.PauseButtonListener {

        @Override
        public void onPause() {
            onGamePaused();
        }

        @Override
        public void onResume() {
            onGameResumed();
        }

    }

    private class GameStartButtonListener implements StartButton.StartButtonListener {

        @Override
        public void onStart() {

        }

    }

    private void onGamePaused() {
        switchState(GameState.PAUSED);
    }

    private void onGameResumed() {
        switchState(GameState.RUNNING);
    }


    private void setupHostOrPlayer() {

        BitmapFont font = new BitmapFont(Gdx.files.internal("default.fnt"));
        Skin skin = new Skin(Gdx.files.internal("uiskin.json"));

        final TextButton host = new TextButton("Host", skin, "default");
        final TextButton play = new TextButton("Player", skin, "default");

        host.setScale(8);
        play.setScale(8);

        if (host.getWidth() > play.getWidth())
            play.setWidth(host.getWidth());
        else
            host.setWidth(play.getWidth());

        float posX = getCamera().viewportWidth / 2 - host.getWidth() - 10;
        float posY = getCamera().viewportHeight * 7 / 8 - gameLabel.getHeight() - host.getHeight() * 2;
        System.out.println("BUTTON : " + posX + "-" + posY);
        host.setPosition(posX, posY);
        play.setPosition(posX + host.getWidth() + 10, posY);

        host.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                host.remove();
                play.remove();
                switchState(GameState.MAIN_MENU);
            }
        });

        play.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                GameManager.getInstance().getGameEventListener().onCharacterSelect();
            }
        });

        addActor(host);
        addActor(play);
    }

    private void switchState(GameState gameState) {
        GameManager.getInstance().setGameState(gameState);
        switch (gameState) {
            case START:
                setUpStageBase();
                setUpCharacters();
                setUpPause();
                setUpTutorial();
                onGameResumed();
                setUpCountDown();
                break;
            case RUNNING:
                break;
            case MAIN_MENU:
                setUpStageBase();
                setUpGameLabel();
                setUpMainMenu();
                break;
            case ABOUT:
                setUpStageBase();
                setUpGameLabel();
                setUpAboutText();
                break;
            case GAME_OVER:
                GameManager.getInstance().resetDifficulty();
                totalTimePassed = 0;
                break;
            case PAUSED:
                break;
            case PICK_MODE:
                setUpStageBase();
                setUpGameLabel();
                setupHostOrPlayer();
                break;
            case WAITING:
                clear();
                setUpStageBase();
                setUpGameLabel();
                gameLabel.changeText("Waiting for players");
                initServer();
                break;
        }
    }

    private Server server;

    private void initServer() {
        server = new Server();
        Kryo kryo = server.getKryo();
        kryo.register(PlayerNetData.class);
        kryo.register(NetMessage.class);
        kryo.register(ShakeData.class);
        server.addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                if (object instanceof PlayerNetData) {
                    PlayerNetData data = (PlayerNetData) object;
                    System.out.println(data.characterName);
                    System.out.println(data.name);
                } else if (object instanceof NetMessage) {
                    NetMessage data = (NetMessage) object;
                    System.out.println(data.getFrom());
                    System.out.println(data.getMessage());
                }
            }

            @Override
            public void connected(Connection connection) {
                System.out.println("connected");
                connection.sendTCP(new NetMessage("server","HELLO WORLD"));
            }

            @Override
            public void disconnected(Connection connection) {

                System.out.println("disconnected");
            }
        });
        server.start();
        try {
            new Thread() {
                @Override
                public void run() {
                    try {
                        server.bind(9999, 9990);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}