package com.antcode.d3ity.shakenram.stages;

import com.antcode.d3ity.shakenram.actors.Background;
import com.antcode.d3ity.shakenram.actors.CountDownTime;
import com.antcode.d3ity.shakenram.actors.Ground;
import com.antcode.d3ity.shakenram.actors.Name;
import com.antcode.d3ity.shakenram.actors.Runner;
import com.antcode.d3ity.shakenram.actors.Runner2;
import com.antcode.d3ity.shakenram.actors.Wall;
import com.antcode.d3ity.shakenram.actors.menu.AboutLabel;
import com.antcode.d3ity.shakenram.actors.menu.GameLabel;
import com.antcode.d3ity.shakenram.actors.menu.Label;
import com.antcode.d3ity.shakenram.actors.menu.MusicButton;
import com.antcode.d3ity.shakenram.actors.menu.PauseButton;
import com.antcode.d3ity.shakenram.actors.menu.PausedLabel;
import com.antcode.d3ity.shakenram.actors.menu.SoundButton;
import com.antcode.d3ity.shakenram.actors.menu.StartButton;
import com.antcode.d3ity.shakenram.actors.menu.Winner;
import com.antcode.d3ity.shakenram.enums.GameState;
import com.antcode.d3ity.shakenram.utils.AudioUtils;
import com.antcode.d3ity.shakenram.utils.BodyUtils;
import com.antcode.d3ity.shakenram.utils.Constants;
import com.antcode.d3ity.shakenram.utils.GameManager;
import com.antcode.d3ity.shakenram.utils.NetMessage;
import com.antcode.d3ity.shakenram.utils.PlayerNetData;
import com.antcode.d3ity.shakenram.utils.ShakeData;
import com.antcode.d3ity.shakenram.utils.WorldUtils;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import java.io.IOException;
import java.util.HashMap;

public class GameStage extends Stage implements ContactListener {

    private static final int VIEWPORT_WIDTH = Constants.APP_WIDTH;
    private static final int VIEWPORT_HEIGHT = Constants.APP_HEIGHT;

    private World world;
    private Ground ground;
    private Runner runner;
    private Runner2 runner2;

    private final float TIME_STEP = 1 / 300f;
    private float accumulator = 0f;

    private OrthographicCamera camera;

    private Rectangle screenLeftSide;
    private Rectangle screenRightSide;

    private SoundButton soundButton;
    private MusicButton musicButton;
    private PauseButton pauseButton;
    private StartButton startButton;

    private CountDownTime countDownTime;
    private float totalTimePassed;
    private boolean tutorialShown;
    private Name title;
    private Winner win;

    private Vector3 touchPoint = new Vector3();

    private GameLabel waitingLabel;
    private Label onlineLabel;

    private boolean sendFlagStart = false;

    public GameStage() {
        super(new ScalingViewport(Scaling.stretch, VIEWPORT_WIDTH, VIEWPORT_HEIGHT,
                new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT)));
        setUpCamera();
        GameManager.dataEvent = new GameManager.IncomingData() {
            @Override
            public void onData(ShakeData data) {

                if (countDownTime != null && countDownTime.isCountOver()) {
                    if (GameManager.getInstance().getGameState() == GameState.RUNNING) {
                        if (runner.getPlayerName().equals(data.from)) {
                            runner.ram();
                        } else if (runner2.getPlayerName().equals(data.from)) {
                            runner2.ram();
                        }
                    }
                }
            }

            @Override
            public void onJoin(PlayerNetData data) {
                if (runner == null)
                    setUpRunner(data.name, data.characterName);
                else if (runner2 == null) {
                    setUpRunner2(data.name, data.characterName);
                    waitingLabel.remove();

                    setUpWinner("Klik untuk mulai");
                }
            }

            @Override
            public void onLeave(PlayerNetData data) {
                if (runner != null && runner.getPlayerName().equals(data.name)) {
                    runner.remove();
                    runner = null;
                } else if (runner2 != null && runner2.getPlayerName().equals(data.name)) {
                    runner2.remove();
                    runner2 = null;
                }
            }

        };
//        setUpTouchControlAreas();
        Gdx.input.setInputProcessor(this);
        AudioUtils.getInstance().init();
        if (Gdx.app.getType() == Application.ApplicationType.Android)
            switchState(GameState.PICK_MODE);
        else
            switchState(GameState.MAIN_MENU);
    }

    private void setUpStageBase() {
        setUpWorld();
        setUpFixedMenu();
    }

    private void setUpGameLabel() {
        if (waitingLabel == null) {
            Rectangle gameLabelBounds = new Rectangle(0, camera.viewportHeight * 7 / 8,
                    camera.viewportWidth, camera.viewportHeight / 4);
            waitingLabel = new GameLabel(gameLabelBounds);
        }
        if(title==null){
            title = new Name();
        }
        waitingLabel.changeText(Constants.GAME_NAME);
        addActor(title);
    }

    private void setUpAboutText() {
        Rectangle gameLabelBounds = new Rectangle(0, camera.viewportHeight * 5 / 8,
                camera.viewportWidth, camera.viewportHeight / 4);
        addActor(new AboutLabel(gameLabelBounds));
    }

    private void setUpFixedMenu() {
        setUpSound();
        setUpMusic();
    }

    private void setUpSound() {
        Rectangle soundButtonBounds = new Rectangle(camera.viewportWidth / 64,
                camera.viewportHeight * 13 / 20, camera.viewportHeight / 10,
                camera.viewportHeight / 10);
        soundButton = new SoundButton(soundButtonBounds);
        addActor(soundButton);
    }

    private void setUpMusic() {
        Rectangle musicButtonBounds = new Rectangle(camera.viewportWidth / 64,
                camera.viewportHeight * 4 / 5, camera.viewportHeight / 10,
                camera.viewportHeight / 10);
        musicButton = new MusicButton(musicButtonBounds);
        addActor(musicButton);
    }

    private void setUpCountDown() {
        Rectangle countDownBound = new Rectangle(0,
                camera.viewportHeight / 4, camera.viewportWidth,
                camera.viewportHeight / 4);
        countDownTime = new CountDownTime(countDownBound);
        addActor(countDownTime);
    }

    private void setUpWinner(String name) {
        Rectangle countDownBound = new Rectangle(0,
                camera.viewportHeight / 2, camera.viewportWidth,
                camera.viewportHeight / 4);
        win = new Winner(countDownBound, name);
        addActor(win);
    }

    private void setUpPause() {
        Rectangle pauseButtonBounds = new Rectangle(camera.viewportWidth / 64,
                camera.viewportHeight * 1 / 2, camera.viewportHeight / 10,
                camera.viewportHeight / 10);
        pauseButton = new PauseButton(pauseButtonBounds, new GamePauseButtonListener());
//        addActor(pauseButton);
    }

    /**
     * These menu buttons are only displayed when the game is over
     */
    private void setUpMainMenu() {
        setUpStart();
        if (win != null) {
            win.remove();
        }
        removeRunner();
    }

    private void removeRunner() {
        if (runner != null) {
            runner.remove();
            runner = null;
        }
        if (runner2 != null) {
            runner2.remove();
            runner2 = null;
        }
        player.clear();
    }

    private void setUpStart() {
        //Setup start button
        if(startButton==null) {
            Rectangle startButtonBounds = new Rectangle(
                    camera.viewportWidth * 6 / 16,
                    camera.viewportHeight / 4,
                    camera.viewportWidth / 4,
                    (camera.viewportWidth / 4) / 786f * 633);
            startButton = new StartButton(startButtonBounds, new GameStartButtonListener());
        }
        addActor(startButton);
    }

    private void setUpWorld() {
        world = WorldUtils.createWorld();
        world.setContactListener(this);
        setUpBackground();
        setUpGround();
    }

    private void setUpBackground() {
        addActor(new Background());
    }

    private void setUpGround() {
        ground = new Ground(WorldUtils.createGround(world));
        addActor(ground);

        addActor(new Wall(WorldUtils.createWallLeft(world)));
        addActor(new Wall(WorldUtils.createWallRight(world)));
    }
//
//    private void setUpCharacters() {
//        setUpPauseLabel();
//        setUpRunner("RAIZ", Constants.STALP);
//        setUpRunner2("NAW", Constants.PANATA);
//        //createEnemy();
//    }

    private void setUpRunner(String name, String charName) {
        if (runner != null) {
            runner.remove();
        }
        runner = new Runner(WorldUtils.createRunner(world), name, charName);

        runner.getBody().setLinearVelocity(0, 0);
        addActor(runner);
    }

    private void setUpRunner2(String name, String charName) {
        if (runner2 != null) {
            runner2.remove();
        }
        runner2 = new Runner2(WorldUtils.createRunner2
                (world), name, charName);

        runner2.getBody().setLinearVelocity(0, 0);
        addActor(runner2);
    }

    private void setUpCamera() {
        camera = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0f);
        camera.update();
    }

    private void setUpTouchControlAreas() {
        touchPoint = new Vector3();
        screenLeftSide = new Rectangle(0, 0, camera.viewportWidth / 2,
                camera.viewportHeight);
        screenRightSide = new Rectangle(camera.viewportWidth / 2, 0,
                camera.viewportWidth / 2, camera.viewportHeight);
    }

    private void setUpPauseLabel() {
        Rectangle pauseLabelBounds = new Rectangle(0, camera.viewportHeight * 7 / 8,
                camera.viewportWidth, camera.viewportHeight / 4);
        addActor(new PausedLabel(pauseLabelBounds));
    }

    private void setUpTutorial() {
        if (tutorialShown) {
            return;
        }
        setUpLeftTutorial();
        setUpRightTutorial();
        tutorialShown = true;
    }

    private void setUpLeftTutorial() {
        float width = camera.viewportHeight / 4;
        float x = camera.viewportWidth / 4 - width / 2;
        Rectangle leftTutorialBounds = new Rectangle(x, camera.viewportHeight * 9 / 20, width,
                width);
    }

    private void setUpRightTutorial() {
        float width = camera.viewportHeight / 4;
        float x = camera.viewportWidth * 3 / 4 - width / 2;
        Rectangle rightTutorialBounds = new Rectangle(x, camera.viewportHeight * 9 / 20, width,
                width);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (runner == null || runner2 == null) return;

        if (GameManager.getInstance().getGameState() == GameState.PAUSED) return;

        if ((countDownTime != null && !countDownTime.isCountOver()))
            return;

        if (GameManager.getInstance().getGameState() == GameState.RUNNING) {

            totalTimePassed += delta;
        }

        Array<Body> bodies = new Array<Body>(world.getBodyCount());
        world.getBodies(bodies);

        for (Body body : bodies) {
            update(body);
        }

        // Fixed timestep
        accumulator += delta;

        while (accumulator >= delta) {
            world.step(TIME_STEP, 6, 2);
            accumulator -= TIME_STEP;
        }

        if (runner2 != null)
            runner2.isBounced();
        if (runner != null)
            runner.isBounced();
        //TODO: Implement interpolation

    }

    private void update(Body body) {
        if (!BodyUtils.bodyInBounds(body)) {
            if (BodyUtils.bodyIsEnemy(body) && !runner.isHit()) {
                //createEnemy();
            }
            world.destroyBody(body);
        }
    }

    @Override
    public boolean touchDown(int x, int y, int pointer, int button) {

        // Need to get the actual coordinates
        translateScreenToWorldCoordinates(x, y);

        // If a menu control was touched ignore the rest
        if (menuControlTouched(touchPoint.x, touchPoint.y)) {
            return super.touchDown(x, y, pointer, button);
        }

        if (GameManager.getInstance().getGameState() != GameState.RUNNING) {
            return super.touchDown(x, y, pointer, button);
        }

        if (countDownTime != null && countDownTime.isCountOver()) {
//            if (rightSideTouched(touchPoint.x, touchPoint.y)) {
//                if (!runner.isHit())
//                    runner.ram();
//            } else if (leftSideTouched(touchPoint.x, touchPoint.y)) {
//                if (!runner2.isHit())
//                    runner2.ram();
//            }
        }

        return super.touchDown(x, y, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (GameManager.getInstance().getGameState() != GameState.RUNNING) {
            return super.touchUp(screenX, screenY, pointer, button);
        }

        return super.touchUp(screenX, screenY, pointer, button);
    }

    private boolean menuControlTouched(float x, float y) {
        boolean touched = false;
        try {
            switch (GameManager.getInstance().getGameState()) {
                case MAIN_MENU:
                    touched = startButton.getBounds().contains(x, y);

                    if (touched) {
                        if (Gdx.app.getType() == Application.ApplicationType.Android) {
//                            switchState(GameState.START);
                            switchState(GameState.WAITING);
                        } else
                            //switchState(GameState.START);
                            switchState(GameState.WAITING);
                    }
                    break;
                case RUNNING:
                    break;
                case PAUSED:
                    touched = pauseButton.getBounds().contains(x, y);
                    break;
                case GAME_OVER:
                    touched = win.getBounds().contains(x, y);
                    if (touched) {
                        switchState(GameState.MAIN_MENU);
                    }
                    break;
                case WAITING:
                    touched = true;//win!=null && win.getBounds().contains(x, y);
                    if (touched && runner != null && runner2 != null) {
                        switchState(GameState.START);
                        if (win != null)
                            win.remove();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return touched || soundButton.getBounds().contains(x, y)
                || musicButton.getBounds().contains(x, y);
    }

    private boolean rightSideTouched(float x, float y) {
        return screenRightSide.contains(x, y);
    }

    private boolean leftSideTouched(float x, float y) {
        return screenLeftSide.contains(x, y);
    }

    /**
     * Helper function to get the actual coordinates in my world
     *
     * @param x
     * @param y
     */
    private void translateScreenToWorldCoordinates(int x, int y) {
        if (camera != null)
            camera.unproject(touchPoint.set(x, y, 0));
    }

    @Override
    public void beginContact(Contact contact) {

        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        if ((BodyUtils.bodyIsRunner(a) && BodyUtils.bodyIsRunner2(b)) ||
                (BodyUtils.bodyIsRunner2(a) && BodyUtils.bodyIsRunner(b))) {
//            runner.hit();
//            runner2.hit();
        } else if ((BodyUtils.bodyIsRunner(a) && BodyUtils.bodyIsWallLeft(b)) ||
                (BodyUtils.bodyIsWallLeft(a) && BodyUtils.bodyIsRunner(b))) {
            setUpWinner(runner2.getPlayerName());
            playerCon.get(runner2.getPlayerName()).sendTCP(new NetMessage("server", "win"));
            playerCon.get(runner.getPlayerName()).sendTCP(new NetMessage("server", "lose"));
            if (GameManager.getInstance().getGameEventListener() != null) {
                GameManager.getInstance().getGameEventListener().onGameFinish(new NetMessage("server", "winner:" + runner2.getName()));
            }
            switchState(GameState.GAME_OVER);
        } else if ((BodyUtils.bodyIsRunner2(a) && BodyUtils.bodyIsWallRight(b)) ||
                (BodyUtils.bodyIsWallRight(a) && BodyUtils.bodyIsRunner2(b))) {
            setUpWinner(runner.getPlayerName());
            playerCon.get(runner.getPlayerName()).sendTCP(new NetMessage("server", "win"));
            playerCon.get(runner2.getPlayerName()).sendTCP(new NetMessage("server", "lose"));
            if (GameManager.getInstance().getGameEventListener() != null) {
                GameManager.getInstance().getGameEventListener().onGameFinish(new NetMessage("server", "winner:" + runner.getName()));
            }
            switchState(GameState.GAME_OVER);
        }
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    private class GamePauseButtonListener implements PauseButton.PauseButtonListener {

        @Override
        public void onPause() {
            onGamePaused();
        }

        @Override
        public void onResume() {
            onGameResumed();
        }

    }

    private class GameStartButtonListener implements StartButton.StartButtonListener {

        @Override
        public void onStart() {

        }

    }

    private void onGamePaused() {
        switchState(GameState.PAUSED);
    }

    private void onGameResumed() {
        switchState(GameState.RUNNING);
    }


    private void setupHostOrPlayer() {

        BitmapFont font = new BitmapFont(Gdx.files.internal("default.fnt"));
        Skin skin = new Skin(Gdx.files.internal("uiskin.json"));

        final TextButton host = new TextButton("Host", skin, "default");
        final TextButton play = new TextButton("Player", skin, "default");

        host.setScale(8);
        play.setScale(8);

        if (host.getWidth() > play.getWidth())
            play.setWidth(host.getWidth());
        else
            host.setWidth(play.getWidth());

        float posX = camera.viewportWidth / 2 - host.getWidth() - 10;
        float posY = camera.viewportHeight * 7 / 8 - waitingLabel.getHeight() - host.getHeight() * 2;

        host.setPosition(posX, posY);
        play.setPosition(posX + host.getWidth() + 10, posY);

        host.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                host.remove();
                play.remove();
                switchState(GameState.MAIN_MENU);
            }
        });

        play.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                GameManager.getInstance().getGameEventListener().onCharacterSelect();
            }
        });

        addActor(host);
        addActor(play);
    }

    private void switchState(GameState gameState) {
        GameManager.getInstance().setGameState(gameState);
        switch (gameState) {
            case START:
                setUpPause();
                onGameResumed();
                setUpCountDown();
                switchState(GameState.COUNTDOWN);
                break;
            case RUNNING:
                if(!sendFlagStart){
                    sendFlagStart = true;
                    for(Connection connection : playerCon.values())
                        connection.sendTCP(new NetMessage("server", "start"));
                }
                break;
            case MAIN_MENU:
                removeConnections();
                sendFlagStart = false;
                initServer();
                setUpStageBase();
                setUpGameLabel();
                setUpMainMenu();
                break;
            case ABOUT:
                setUpStageBase();
                setUpGameLabel();
                setUpAboutText();
                break;
            case GAME_OVER:
                GameManager.getInstance().resetDifficulty();
                totalTimePassed = 0;
                removeConnections();
                break;
            case PAUSED:
                break;
            case PICK_MODE:
                setUpStageBase();
                setUpGameLabel();
                setupHostOrPlayer();
                break;
            case WAITING:
                clear();
                setUpStageBase();
                waitingLabel.changeText("Waiting for players");
                addActor(waitingLabel);
                break;
        }
    }

    private void removeConnections(){
        try {
            for (Connection con : playerCon.values())
                con.close();
            player.clear();
            playerCon.clear();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private Server server;

    private HashMap<String, String> player = new HashMap<String, String>();
    private HashMap<String, Connection> playerCon = new HashMap<String, Connection>();

    private void initServer() {
        if (server == null) {
            server = new Server();
            Kryo kryo = server.getKryo();
            kryo.register(PlayerNetData.class);
            kryo.register(NetMessage.class);
            kryo.register(ShakeData.class);
            server.addListener(new Listener() {

                @Override
                public void received(Connection connection, Object object) {
                    if (object instanceof PlayerNetData) {
                        PlayerNetData data = (PlayerNetData) object;
                        if (GameManager.getInstance().getGameState() == GameState.WAITING) {
                            GameManager.dataEvent.onJoin(data);
                            player.put(connection.getRemoteAddressUDP().getHostName(), data.name);
                            playerCon.put(data.name, connection);
                        }
                        System.out.println(data.characterName);
                        System.out.println(data.name);
                    } else if (object instanceof NetMessage) {
                        NetMessage data = (NetMessage) object;
                        System.out.println(data.getFrom());
                        System.out.println(data.getMessage());
                    } else if (object instanceof ShakeData) {
                        ShakeData data = (ShakeData) object;
                        System.out.println(data.from + " Shaking~~");
                        if (GameManager.getInstance().getGameState() == GameState.RUNNING) {
                            GameManager.dataEvent.onData(data);
                        }
                    }
                }

                @Override
                public void connected(Connection connection) {
                    if (playerCon.size() >= 2) {
                        System.out.println(connection.getRemoteAddressTCP().getHostName() + " kicked: too many players");
                        connection.sendTCP(new NetMessage("server", "kicked"));
                        connection.close();
                    } else {
                        System.out.println("connected");
                    }

                }

                @Override
                public void disconnected(Connection connection) {
                    try {
                        PlayerNetData data = new PlayerNetData();
                        if (player.containsKey(connection.getRemoteAddressUDP().getHostName())) {
                            data.name = player.get(connection.getRemoteAddressUDP().getHostName());
                            GameManager.dataEvent.onLeave(data);
                            player.remove(connection.getRemoteAddressUDP().getHostName());
                            playerCon.remove(data.name);
                        }
                        System.out.println("disconnected");
                        if (GameManager.getInstance().getGameState() == GameState.WAITING || GameManager.getInstance().getGameState() == GameState.START || GameManager.getInstance().getGameState() == GameState.RUNNING || GameManager.getInstance().getGameState() == GameState.COUNTDOWN) {
                            for (Connection c : playerCon.values()) {
                                c.sendTCP(new NetMessage("from", "cancel"));
                            }
                            removeConnections();
                            switchState(GameState.MAIN_MENU);
                        }
                    } catch (Exception e) {

                    }
                }
            });

            server.start();
            try {
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            server.bind(9999, 9990);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public OrthographicCamera getCamera() {
        return camera;
    }
}