package com.antcode.d3ity.shakenram.utils;

public class NetMessage {
    private String from;
    private String message;

    public NetMessage(){}

    public NetMessage(String from, String message) {
        this.from = from;
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
