/*
 * Copyright (c) 2014. William Mora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.antcode.d3ity.shakenram.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.antcode.d3ity.shakenram.enums.Difficulty;
import com.antcode.d3ity.shakenram.enums.GameState;

/**
 * A utility singleton that holds the current {@link com.antcode.d3ity.shakenram.enums.Difficulty}
 * and {@link com.antcode.d3ity.shakenram.enums.GameState} of the game as well as the
 * {@link com.antcode.d3ity.shakenram.utils.GameEventListener} instance responsible for dispatching
 * all game events for the platform running the game
 */
public class GameManager{
    private static GameManager ourInstance = new GameManager();

    public static final String PREFERENCES_NAME = "preferences";
    private static final String MAX_SCORE_PREFERENCE = "max_score";
    private static final String ACHIEVEMENT_COUNT_PREFERENCE_SUFFIX = "_count";
    private static final String ACHIEVEMENT_UNLOCKED_PREFERENCE_SUFFIX = "_unlocked";

    private GameState gameState;
    private Difficulty difficulty;
    private GameEventListener gameEventListener;

    public interface IncomingData{
        public void onData(ShakeData data);

        public void onJoin(PlayerNetData data);

        public void onLeave(PlayerNetData data);
    }

    public static IncomingData dataEvent;

    public static GameManager getInstance() {
        return ourInstance;
    }

    private GameManager() {
        gameState = GameState.GAME_OVER;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public boolean isMaxDifficulty() {
        return difficulty == Difficulty.values()[Difficulty.values().length - 1];
    }

    public void resetDifficulty() {
        setDifficulty(Difficulty.values()[0]);
    }

    public void setGameEventListener(GameEventListener gameEventListener) {
        this.gameEventListener = gameEventListener;
    }


    public GameEventListener getGameEventListener() {
        return gameEventListener;
    }

    private Preferences getPreferences() {
        return Gdx.app.getPreferences(PREFERENCES_NAME);
    }

    public void saveScore(int score) {
        Preferences preferences = getPreferences();
        int maxScore = preferences.getInteger(MAX_SCORE_PREFERENCE, 0);
        if (score > maxScore) {
            preferences.putInteger(MAX_SCORE_PREFERENCE, score);
            preferences.flush();
        }
    }

    public boolean hasSavedMaxScore() {
        return getPreferences().getInteger(MAX_SCORE_PREFERENCE, 0) > 0;
    }

    public void setAchievementUnlocked(String id) {
        getPreferences().putBoolean(getAchievementUnlockedId(id), true);
    }

    public void incrementAchievementCount(String id, int steps) {
        Preferences preferences = getPreferences();
        int count = preferences.getInteger(getAchievementCountId(id), 0);
        count += steps;
        preferences.putInteger(getAchievementCountId(id), count);
        preferences.flush();
    }

    private int getAchievementCount(String id) {
        return getPreferences().getInteger(getAchievementCountId(id), 0);
    }

    private boolean isAchievementUnlocked(String id) {
        return getPreferences().getBoolean(getAchievementUnlockedId(id), false);
    }

    private String getAchievementCountId(String id) {
        return id + ACHIEVEMENT_COUNT_PREFERENCE_SUFFIX;
    }

    private String getAchievementUnlockedId(String id) {
        return id + ACHIEVEMENT_UNLOCKED_PREFERENCE_SUFFIX;
    }

}
