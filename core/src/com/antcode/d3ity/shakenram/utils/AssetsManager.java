/*
 * Copyright (c) 2014. William Mora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.antcode.d3ity.shakenram.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.util.HashMap;

public class AssetsManager {

    private static HashMap<String, TextureRegion> texturesMap = new HashMap<String, TextureRegion>();
    private static HashMap<String, Animation> animationsMap = new HashMap<String, Animation>();
    private static TextureAtlas textureAtlas, menuTextureAtlas;
    private static BitmapFont smallFont;
    private static BitmapFont smallestFont;
    private static BitmapFont largeFont;
    public static GlyphLayout text = new GlyphLayout();
    public static Texture name;

    private AssetsManager() {

    }

    public static void loadAssets() {
        // Background
        texturesMap.put(Constants.BACKGROUND_ASSETS_ID,
                new TextureRegion(new Texture(Gdx.files.internal(Constants.BACKGROUND_IMAGE_PATH))));

        // Name
        name = new Texture(Gdx.files.internal(Constants.NAME_IMAGE_PATH));

        // Ground
        texturesMap.put(Constants.GROUND_ASSETS_ID,
                new TextureRegion(new Texture(Gdx.files.internal(Constants.GROUND_IMAGE_PATH))));


        textureAtlas = new TextureAtlas(Constants.SPRITES_ATLAS_PATH);
        menuTextureAtlas = new TextureAtlas(Constants.SPRITES_MENU_ATLAS_PATH);

        texturesMap.put(Constants.BURLAC, textureAtlas.findRegion(Constants.BURLAC, 0));
        texturesMap.put(Constants.GALBEN, textureAtlas.findRegion(Constants.GALBEN, 0));
        texturesMap.put(Constants.KING, textureAtlas.findRegion(Constants.KING, 0));
        texturesMap.put(Constants.MARTIN, textureAtlas.findRegion(Constants.MARTIN, 0));
        texturesMap.put(Constants.PANATA, textureAtlas.findRegion(Constants.PANATA, 0));
        texturesMap.put(Constants.STALP, textureAtlas.findRegion(Constants.STALP, 0));
        texturesMap.put(Constants.TREI, textureAtlas.findRegion(Constants.TREI, 0));

        animationsMap.put(Constants.BURLAC, createAnimation(textureAtlas, Constants.BURLAC));
        animationsMap.put(Constants.GALBEN, createAnimation(textureAtlas, Constants.GALBEN));
        animationsMap.put(Constants.KING, createAnimation(textureAtlas, Constants.KING));
        animationsMap.put(Constants.MARTIN, createAnimation(textureAtlas, Constants.MARTIN));
        animationsMap.put(Constants.PANATA, createAnimation(textureAtlas, Constants.PANATA));
        animationsMap.put(Constants.STALP, createAnimation(textureAtlas, Constants.STALP));
        animationsMap.put(Constants.TREI, createAnimation(textureAtlas, Constants.TREI));

        texturesMap.put("judul",
                new TextureRegion(new Texture(Gdx.files.internal("judul.png"))));

        // Tutorial
        texturesMap.put(Constants.TUTORIAL_LEFT_REGION_NAME,
                textureAtlas.findRegion(Constants.TUTORIAL_LEFT_REGION_NAME));
        texturesMap.put(Constants.TUTORIAL_RIGHT_REGION_NAME,
                textureAtlas.findRegion(Constants.TUTORIAL_RIGHT_REGION_NAME));

        // Fonts
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(Constants.FONT_NAME));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 36;
        smallFont = generator.generateFont(parameter);
        smallFont.setColor(.21f, .22f, .21f, 1f);
        parameter.size = 72;
        largeFont = generator.generateFont(parameter);
        largeFont.setColor(.21f, .22f, .21f, 1f);
        parameter.size = 9;
        smallestFont = generator.generateFont(parameter);
        smallestFont.setColor(.21f, .22f, .21f, 1f);
        generator.dispose();

    }

    public static TextureRegion getTextureRegion(String key) {
        return texturesMap.get(key);
    }

    public static Animation getAnimation(String key) {
        return animationsMap.get(key);
    }

    private static Animation createAnimation(TextureAtlas textureAtlas, String regionName) {

        TextureRegion[] runningFrames = new TextureRegion[3];

        for (int i = 1; i <= runningFrames.length; i++) {
            runningFrames[i - 1] = textureAtlas.findRegion(regionName, i);
        }

        return new Animation(0.1f, runningFrames);

    }

    public static TextureAtlas getTextureAtlas() {
        return textureAtlas;
    }

    public static TextureAtlas getMenuTextureAtlas() {
        return menuTextureAtlas;
    }

    public static BitmapFont getSmallFont() {
        return smallFont;
    }

    public static BitmapFont getLargeFont() {
        if (largeFont == null) {
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(Constants.FONT_NAME));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 36;
            largeFont = generator.generateFont(parameter);
            largeFont.setColor(.21f, .22f, .21f, 1f);
            parameter.size = 9;
        }
        return largeFont;
    }

    public static BitmapFont getSmallestFont() {
        return smallestFont;
    }

    public static void dispose() {
        textureAtlas.dispose();
        smallestFont.dispose();
        smallFont.dispose();
        largeFont.dispose();
        texturesMap.clear();
        animationsMap.clear();
    }
}
