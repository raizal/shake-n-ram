package com.antcode.d3ity.shakenram.actors;

import com.antcode.d3ity.shakenram.box2d.GroundUserData;
import com.antcode.d3ity.shakenram.box2d.WallUserData;
import com.antcode.d3ity.shakenram.enums.GameState;
import com.antcode.d3ity.shakenram.utils.AssetsManager;
import com.antcode.d3ity.shakenram.utils.Constants;
import com.antcode.d3ity.shakenram.utils.GameManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by root on 09/10/15.
 */
public class Wall extends GameActor {

    public Wall(Body body) {
        super(body);
    }

    @Override
    public WallUserData getUserData() {
        return (WallUserData) userData;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (GameManager.getInstance().getGameState() != GameState.RUNNING) {
            return;
        }
    }

}
