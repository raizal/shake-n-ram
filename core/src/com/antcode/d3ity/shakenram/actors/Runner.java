/*
 * Copyright (c) 2014. William Mora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.antcode.d3ity.shakenram.actors;

import com.antcode.d3ity.shakenram.box2d.Runner2UserData;
import com.antcode.d3ity.shakenram.box2d.RunnerUserData;
import com.antcode.d3ity.shakenram.enums.Difficulty;
import com.antcode.d3ity.shakenram.enums.GameState;
import com.antcode.d3ity.shakenram.utils.AssetsManager;
import com.antcode.d3ity.shakenram.utils.AudioUtils;
import com.antcode.d3ity.shakenram.utils.Constants;
import com.antcode.d3ity.shakenram.utils.GameManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Align;

public class Runner extends GameActor {

    protected boolean hit;
    protected Animation runningAnimation;
    protected float stateTime;

    protected Sound jumpSound;
    protected Sound hitSound;

    private BitmapFont font = AssetsManager.getSmallestFont();

    protected int jumpCount;

    private String playerName;

    public Runner(Body body,String name,String characterName) {
        super(body);
        playerName = name;
        jumpCount = 0;
        runningAnimation = AssetsManager.getAnimation(characterName);
        stateTime = 0f;
        jumpSound = AudioUtils.getInstance().getJumpSound();
        hitSound = AudioUtils.getInstance().getHitSound();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        float x = screenRectangle.x - (screenRectangle.width * 0.1f);
        float y = screenRectangle.y;

        // Running
        if (GameManager.getInstance().getGameState() == GameState.RUNNING) {
            stateTime += Gdx.graphics.getDeltaTime();
        }

        TextureRegion texture;
        if(body.getLinearVelocity().x!=0)
            texture = runningAnimation.getKeyFrame(stateTime, true);
        else
            texture = runningAnimation.getKeyFrame(0, true);

        float scale = screenRectangle.height/texture.getRegionHeight();
        float width = scale * texture.getRegionWidth();
        float height = scale * texture.getRegionHeight();

        batch.draw(texture,x,y,width,height);
        font.draw(batch,playerName,x,y-5,width, Align.center,true);
    }

    @Override
    public Runner2UserData getUserData() {
        return (Runner2UserData) userData;
    }

    public void hit() {
        body.setLinearVelocity(Constants.BOUNCE_POWER,0);
        hit = true;
        AudioUtils.getInstance().playSound(hitSound);
    }

    public boolean isHit() {
        return hit;
    }

//    private float ramVelocity = -1f;

    public void ram(){
        body.applyForceToCenter(new Vector2(800,0),true);
    }

    public void onDifficultyChange(Difficulty newDifficulty) {
        setGravityScale(newDifficulty.getRunnerGravityScale());
        getUserData().setJumpingLinearImpulse(newDifficulty.getRunnerJumpingLinearImpulse());
    }

    public void setGravityScale(float gravityScale) {
        body.setGravityScale(gravityScale);
        body.resetMassData();
    }

    public int getJumpCount() {
        return jumpCount;
    }


    public boolean isBounced(){
        if(hit){
            if(body.getLinearVelocity().x==0){
                hit = false;
                return false;
            }
            return true;
        }
        return false;
    }

    public String getPlayerName() {
        return playerName;
    }
}