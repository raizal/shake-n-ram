/*
 * Copyright (c) 2014. William Mora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.antcode.d3ity.shakenram.actors.menu;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.antcode.d3ity.shakenram.utils.AssetsManager;
import com.antcode.d3ity.shakenram.utils.Constants;
import com.badlogic.gdx.utils.Align;

public class Label extends Actor {

    private BitmapFont font;
    private String text = Constants.GAME_NAME;

    public Label(String text,BitmapFont font) {
        this.font = font;
        this.text = text;
        setWidth(getTextWidth());
        setHeight(getTextHeight());
    }

    public float getTextWidth(){
        AssetsManager.text.setText(font,text);
        return AssetsManager.text.width;
    }

    public float getTextHeight(){
        AssetsManager.text.setText(font,text);
        return AssetsManager.text.height;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        AssetsManager.text.setText(font, text);
        font.draw(batch, AssetsManager.text, getX(), getY());
    }

    public void reset(){
        text = Constants.GAME_NAME;
    }

    public void changeText(String text){
        this.text = text;
    }

}