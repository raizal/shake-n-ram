/*
 * Copyright (c) 2014. William Mora
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.antcode.d3ity.shakenram.actors;


import com.antcode.d3ity.shakenram.enums.GameState;
import com.antcode.d3ity.shakenram.utils.AssetsManager;
import com.antcode.d3ity.shakenram.utils.GameManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;

public class CountDownTime extends Actor {

    private Rectangle bounds;
    private BitmapFont font;

    private float time = 3.4f;

    private long startTime = 3;

    private boolean start = false;

    public CountDownTime(Rectangle bounds) {
        this.bounds = bounds;
        setWidth(bounds.width);
        setHeight(bounds.height);
        font = AssetsManager.getLargeFont();
        startTime = 3;
    }

    public void start() {
        start = true;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (GameManager.getInstance().getGameState() != GameState.COUNTDOWN) {
            return;
        }
//        if(start) {
        if (time > 0.1f) {
            time -= delta; //if counting down
        } else {
            GameManager.getInstance().setGameState(GameState.RUNNING);
            remove();
        }
//        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
//        if(!start)
//            font.draw(batch,"Waiting for players...", bounds.x, bounds.y-25, bounds.width, Align.center, true);
//        else
        font.draw(batch, String.format("%d", (int) Math.floor(time)), bounds.x, bounds.y, bounds.width, Align.center, true);
    }

    public boolean isCountOver() {
        return (time < 1);
    }
}
