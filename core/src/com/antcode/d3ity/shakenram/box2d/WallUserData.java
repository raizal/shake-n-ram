package com.antcode.d3ity.shakenram.box2d;

import com.antcode.d3ity.shakenram.enums.UserDataType;

/**
 * Created by root on 09/10/15.
 */

public class WallUserData extends UserData {

    public WallUserData(boolean left) {
        userDataType = left?UserDataType.WALL_LEFT:UserDataType.WALL_RIGHT;
    }

}