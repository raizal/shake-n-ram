package com.antcode.d3ity.shakenram.android;

public interface StepListener {
    public void onStep();
}