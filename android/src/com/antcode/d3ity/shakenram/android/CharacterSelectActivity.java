package com.antcode.d3ity.shakenram.android;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class CharacterSelectActivity extends AppCompatActivity {

    public static CharacterSelectActivity instance;

    @InjectView(R.id.pager)
    ViewPager pager;
    @InjectView(R.id.name)
    EditText name;
    @InjectView(R.id.join)
    Button join;

    @InjectView(R.id.host)
    EditText host;
    static EditText hostStatic;
    @InjectView(R.id.step)
    TextView step;
    @InjectView(R.id.container)
    RelativeLayout container;

    private boolean starting = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
        setContentView(R.layout.character_select);
        ButterKnife.inject(this);
        hostStatic = host;
        host.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog d = new DialogSearchHost(CharacterSelectActivity.this);
                d.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        CharacterSelectActivity.this.onResume();
                    }
                });
                d.show();
            }
        });
        pager.setAdapter(new Adapter(getSupportFragmentManager()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        String nama = getSharedPreferences("conf", MODE_PRIVATE).getString("name", "Player");
        String host = getSharedPreferences("conf", MODE_PRIVATE).getString("host", "192.168.0.1");
        name.setText(nama);
        this.host.setText(host);
    }

    public void win() {
        starting = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                container.setVisibility(View.VISIBLE);
                step.setVisibility(View.GONE);
                step.setText("0");
            }
        });
    }

    public void lose() {
        starting = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                container.setVisibility(View.VISIBLE);
                step.setVisibility(View.GONE);
                step.setText("0");
            }
        });
    }

    public void cancel() {
        starting = false;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    container.setVisibility(View.VISIBLE);
                    step.setVisibility(View.GONE);
                    step.setText("0");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public void start() {
        starting = true;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                container.setVisibility(View.GONE);
                step.setVisibility(View.VISIBLE);
                step.setText("0");
            }
        });
    }

    public void stepping(final int count) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                step.setText(count + "");
            }
        });
    }

    @OnClick(R.id.join)
    protected void join() {
        getSharedPreferences("conf", MODE_PRIVATE)
                .edit()
                .putString("name", name.getText().toString())
                .putString("host", host.getText().toString())
                .commit();

        StepService.name = name.getText().toString();
        StepService.charName = Constants.characters[pager.getCurrentItem()];
        StepService.getInstance(this).join();
    }

    private class Adapter extends FragmentStatePagerAdapter {

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharViewFragment getItem(int position) {
            return
                    CharViewFragment.newInstance(
                            Constants.characters[position],
                            getResources().getDrawable(Constants.charactersId[position])
                    );
        }

        @Override
        public int getCount() {
            return Constants.characters.length;
        }
    }

    public static void showMessage(final String msg) {
        instance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(instance, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(starting){
            StepService.getInstance(this).leave();
        } else
            super.onBackPressed();
    }
}
