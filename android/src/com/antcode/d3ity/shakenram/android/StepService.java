package com.antcode.d3ity.shakenram.android;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;

import com.antcode.d3ity.shakenram.utils.NetMessage;
import com.antcode.d3ity.shakenram.utils.PlayerNetData;
import com.antcode.d3ity.shakenram.utils.ShakeData;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.util.ArrayList;
import java.util.List;

public class StepService extends Service {

    public static String charName;
    public static String name;

    private static SensorManager mSensorManager;
    private static Sensor mSensor;
    private Client client;
    private boolean playing = false;
    private static StepService instance;

    private static int step = 0;
    private  static Vibrator vibrator;
    private static Context context;

    public static StepService getInstance(Context context) {
        StepService.context = context;
        if (instance == null) {
            instance = new StepService();
            mSensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        step = 0;
    }

    public StepService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        join();
        return START_STICKY;
    }

    private StepDetector stepDetector = new StepDetector(new StepListener() {
        @Override
        public void onStep() {
            Log.e("SENSOR CEK","STEPPING");
            if (playing) {
                step++;
                try {

                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                ShakeData data = new ShakeData();
                                data.from = name;
                                data.shake = 1;
                                CharacterSelectActivity.instance.stepping(step);
                                client.sendTCP(data);
                                playBeep();
                                if (vibrator != null) {
                                    vibrator.vibrate(50);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    });

    private String host = "localhost";


    public Client getClient() {
        return client;
    }

    public void leave() {
        if (client != null) {
            try {
                mSensorManager.unregisterListener(stepDetector);
                client.close();
                client.stop();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                client = null;
            }
        }
    }

    public void join() {
        mSensorManager.registerListener(stepDetector, mSensor, SensorManager.SENSOR_DELAY_GAME);
        playing = false;
        final PlayerNetData data = new PlayerNetData();
        step = 0;
        data.characterName = charName;
        data.name = name;

        try {
            if (CharacterSelectActivity.hostStatic != null) {
                host = CharacterSelectActivity.hostStatic.getText().toString();
            }
            if (client == null) {
                client = new Client();
                Kryo kryo = client.getKryo();
                kryo.register(PlayerNetData.class);
                kryo.register(NetMessage.class);
                kryo.register(ShakeData.class);
                client.addListener(new Listener() {
                    @Override
                    public void received(Connection connection, Object object) {
                        if (object instanceof NetMessage) {
                            NetMessage msg = (NetMessage) object;

                            if (msg.getMessage().equals("win")) {
                                playing = false;
                                CharacterSelectActivity.instance.win();
                            } else if (msg.getMessage().equals("lose")) {
                                playing = false;
                                CharacterSelectActivity.instance.lose();
                            } else if (msg.getMessage().equals("kicked")) {
                                playing = false;
                                CharacterSelectActivity.instance.cancel();
                            } else if (msg.getMessage().equals("start")) {
                                playing = true;
                            } else if (msg.getMessage().equals("join")) {
                                playing = false;
                                CharacterSelectActivity.instance.start();
                            } else if (msg.getMessage().equals("cancel")) {
                                playing = false;
                                CharacterSelectActivity.instance.cancel();
                            }
                            CharacterSelectActivity.showMessage(msg.getMessage());
                        }
                    }

                    @Override
                    public void connected(Connection connection) {
                        CharacterSelectActivity.showMessage("conneccted");
                        CharacterSelectActivity.instance.start();
                    }

                    @Override
                    public void disconnected(Connection connection) {
                        CharacterSelectActivity.showMessage("disconneccted");
                        CharacterSelectActivity.instance.cancel();
                    }
                });
            }
            client.start();
            if (!client.isConnected()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = null;
                        try {
                            client.connect(5000, host, 9999, 9990);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            if (client.isConnected()) {
                                client.sendTCP(data);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }).start();
            } else {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            client.sendTCP(data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    ;
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<MediaPlayer> mp = new ArrayList<>();
    private AssetFileDescriptor descriptor;

    public void playBeep() {
        try {
            if (descriptor == null)
                descriptor = getAssets().openFd("step.wav");
            if (mp.size() == 0) {
                MediaPlayer m = new MediaPlayer();
                mp.add(m);
                m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                m.prepare();
                m.setVolume(1f, 1f);
                m.setLooping(false);
                m.start();
                return;
            } else {
                for (MediaPlayer m : mp) {
                    if (!m.isPlaying()) {
                        m.start();
                        return;
                    }
                }

                MediaPlayer m = new MediaPlayer();
                mp.add(m);
                m.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                m.prepare();
                m.setVolume(1f, 1f);
                m.setLooping(false);
                m.start();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
