package com.antcode.d3ity.shakenram.android;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by root on 10/10/15.
 */
public class CharViewFragment extends Fragment {

    @InjectView(R.id.view)
    ImageView view;
    @InjectView(R.id.name)
    TextView name;

    private String nameStr;
    private Drawable bitmap;

    public CharViewFragment() {
    }

    public static CharViewFragment newInstance(String name,Drawable bitmap){
        Bundle b = new Bundle();
        CharViewFragment f = new CharViewFragment();
        f.nameStr = name;
        f.bitmap = bitmap;

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_char_view, container, false);
        ButterKnife.inject(this, view);

        this.view.setImageDrawable(bitmap);
        name.setText(nameStr);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
