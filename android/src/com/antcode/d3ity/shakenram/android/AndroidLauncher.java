package com.antcode.d3ity.shakenram.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import com.antcode.d3ity.shakenram.ShakeNRam;
import com.antcode.d3ity.shakenram.utils.GameEventListener;
import com.antcode.d3ity.shakenram.utils.NetMessage;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication implements GameEventListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useWakelock = true;
        setContentView(initializeForView(new ShakeNRam(this), config));
    }

    @Override
    public void onGameWaiting() {
    }

    @Override
    public void onGameClosed() {
    }

    @Override
    public void onGameFinish(NetMessage winner) {
    }

    @Override
    public void onCharacterSelect() {
        Intent intent = new Intent(this, CharacterSelectActivity.class);
        startActivity(intent);
    }
}
