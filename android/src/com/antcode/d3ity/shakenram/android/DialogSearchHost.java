package com.antcode.d3ity.shakenram.android;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.esotericsoftware.kryonet.Client;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by raizal.pregnanta on 20/11/2015.
 */
public class DialogSearchHost extends Dialog {

    @InjectView(R.id.listview)
    ListView listview;

    protected DialogSearchHost(Context context) {
        super(context);
    }

    protected DialogSearchHost(Context context, int theme) {
        super(context, theme);
    }

    protected DialogSearchHost(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_search_host);
        setCancelable(true);
        setTitle("Mencari host...");
        ButterKnife.inject(this);
        final List<String> h = new ArrayList<>();

        listview.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, h));
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getContext().getSharedPreferences("conf", Context.MODE_PRIVATE)
                        .edit()
                        .putString("host", h.get(position))
                        .commit();
                dismiss();
            }
        });

        new AsyncTask<Void,Void,Void>(){
            @Override
            protected Void doInBackground(Void... params) {
                Client client = new Client();
                List<InetAddress> hosts = client.discoverHosts(9990, 1500);
                for (InetAddress ia : hosts) {
                    h.add(ia.getHostName());
                    System.out.println(ia.getHostName());
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                ((BaseAdapter)listview.getAdapter()).notifyDataSetChanged();
            }
        }.execute();
    }

}
